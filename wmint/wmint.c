/*

 wmint.c
 Interrupts Monitor
 by S�bastien LIENARD (slienard@worldnet.fr), GCU (http://real1.cie.fr/gcu)
 
 heavily based on wminet by Dave Clark (clarkd@skyia.com) and Antoine
 Nulle (warp@xs4all.nl) and  wmifs by Martijn Pieterse (pieterse@xs4all.nl).

 see http://windowmaker.mezaway.org for more awesome wm dock apps :)

*/


#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/utsname.h>

#include <X11/Xlib.h>
#include <X11/xpm.h>
#include <X11/extensions/shape.h>

#include "../wmgeneral/wmgeneral.h"
#include "../wmgeneral/misc.h"

#include "wmint-master.xpm"
#include "wmint-mask.xbm"

#define MY_EMAIL "slienard@worldnet.fr"
#define GCU_HOME "http://real1.cie.fr/gcu"
#define WMINT_VERSION "0.8"

char	*ProgName;

typedef struct {
      int bit;
      int irqs;
      int lastirqs;
      char *label;
      } INTERRUPT;

INTERRUPT interrupts[16];

typedef struct {
      int major;
      int minor;
      int rev;
      } LINUXVER;

LINUXVER linuxver = {0,0,0};

void usage(void);
void printVersion(void);
void printLicence(void);
void BlitString(char *name, int x, int y);
void BlitNum(int num, int x, int y);
void DisplayLabel(char *label);
void (*GetDatas)(void);
void GetDatas20(void);
void GetDatas22(void);
void GetRelease(LINUXVER *lver);
void wmint_routine(int, char **, int);

int main(int argc, char *argv[]) {

	int i, interrupt=0;
	char *endp=NULL;
	
	/* Parse Command Line */

	ProgName = argv[0];
	if (strlen(ProgName) >= 5)
		ProgName += (strlen(ProgName) - 5);
	
	for (i=1; i<argc; i++) {
		char *arg = argv[i];

		if (*arg=='-') {
			switch (arg[1]) {
			case 'd' :
				if (strcmp(arg+1, "display")) 
				  {
				     usage();
				     exit(1);
				  }
				break;
                        case 'i' :
				if (++i==argc) 
				  { 
				    usage(); 
				    exit(1); 
				  };
				interrupt=strtol(argv[i], &endp, 10);
				if (*endp || interrupt<0 || interrupt>15) 
				  {
				    usage();
				    exit(1);
				  };
				break;
			case 'v' :
				printVersion();
				exit(0);
				break;
			default:
				usage();
				exit(0);
				break;
			}
		}
	}

	wmint_routine(argc, argv, interrupt);
	return(0);
}

/*******************************************************************************\
|* wmint_routine														   *|
\*******************************************************************************/

void wmint_routine(int argc, char **argv, int index)
{
    XEvent Event;
    int i, but_stat = -1;

    printLicence();

    GetRelease(&linuxver);
    printf (" Linux version %i.%i.%i\n", linuxver.major, linuxver.minor, linuxver.rev);

    if ( linuxver.major == 2 )
       {
         if ( linuxver.minor >= 1 ) GetDatas = GetDatas22;
         else GetDatas = GetDatas20;
       }
    else
       { 
         printf ("\nSorry, but your kernel version isn't supported by wmint.\n");
         exit (1);
       }

    for ( i=0 ; i<16 ; i++) 
      {
        interrupts[i].irqs = 0;
      }


    openXwindow(argc, argv, wmint_master_xpm, wmint_mask_bits, wmint_mask_width, wmint_mask_height);

    // Display LEDs
    copyXPMArea(81, 66, 4, 4,  5,  5);
    copyXPMArea(81, 66, 4, 4, 12,  5);
    copyXPMArea(81, 66, 4, 4, 19,  5);
    copyXPMArea(81, 66, 4, 4, 26,  5);
    copyXPMArea(81, 66, 4, 4, 33,  5);
    copyXPMArea(81, 66, 4, 4, 40,  5);
    copyXPMArea(81, 66, 4, 4, 47,  5);
    copyXPMArea(81, 66, 4, 4, 54,  5);
    copyXPMArea(81, 66, 4, 4,  5, 12);
    copyXPMArea(81, 66, 4, 4, 12, 12);
    copyXPMArea(81, 66, 4, 4, 19, 12);
    copyXPMArea(81, 66, 4, 4, 26, 12);
    copyXPMArea(81, 66, 4, 4, 33, 12);
    copyXPMArea(81, 66, 4, 4, 40, 12);
    copyXPMArea(81, 66, 4, 4, 47, 12);
    copyXPMArea(81, 66, 4, 4, 54, 12);

    // Add buttons on LEDs
    AddMouseRegion( 0,  5,  5, 10, 10);
    AddMouseRegion( 1, 12,  5, 17, 10);
    AddMouseRegion( 2, 19,  5, 24, 10);
    AddMouseRegion( 3, 26,  5, 31, 10);
    AddMouseRegion( 4, 33,  5, 38, 10);
    AddMouseRegion( 5, 40,  5, 45, 10);
    AddMouseRegion( 6, 47,  5, 52, 10);
    AddMouseRegion( 7, 54,  5, 59, 10);
    AddMouseRegion( 8,  5, 12, 10, 17);
    AddMouseRegion( 9, 12, 12, 17, 17);
    AddMouseRegion(10, 19, 12, 24, 17);
    AddMouseRegion(11, 26, 12, 31, 17);
    AddMouseRegion(12, 33, 12, 38, 17);
    AddMouseRegion(13, 40, 12, 45, 17);
    AddMouseRegion(14, 47, 12, 52, 17);
    AddMouseRegion(15, 54, 12, 59, 17);

    // Add button on label
    AddMouseRegion(16,  5, 21, 59, 45);

    RedrawWindow();

    GetDatas();

    // Display interrupt number (to avoid empty field on startup)
    BlitNum (index, 52, 21);

    while (1)
    {
            
       // Get data to display
       GetDatas();
 
       // Display the stuff

       // Display interrupt label
       DisplayLabel (interrupts[index].label);

       // Clear interrupt's number zone
       copyXPMArea(4, 84, 55, 10, 4, 49);

       // Display interrupt's number
       BlitNum (interrupts[index].irqs, 52, 49); 

       // Update LEDs
       for (i=0; i<8; i++)
         {
          if (interrupts[i].bit) copyXPMArea(87, 66, 4, 4, (5+i*7), 5);
          else copyXPMArea(81, 66, 4, 4,  (5+i*7), 5);
         }
       for (; i<16; i++)
         {
          if (interrupts[i].bit) copyXPMArea(87, 66, 4, 4, (7*i-51), 12);
          else copyXPMArea(81, 66, 4, 4,  (7*i-51), 12);
         }

       RedrawWindow();
    
        
        // X Events
        while (XPending(display))
        {
            XNextEvent(display, &Event);
            switch (Event.type)
                {
		case Expose:
			RedrawWindow();
			break;
		case DestroyNotify:
			XCloseDisplay(display);
			exit(0);
                        break;
		case ButtonPress:
			i = CheckMouseRegion(Event.xbutton.x, Event.xbutton.y);
			but_stat = i;
			break;
		case ButtonRelease:
			i = CheckMouseRegion(Event.xbutton.x, Event.xbutton.y);
                        if (but_stat == i && but_stat >= 0)
                          {
                            // A button has been pressed

                            if (but_stat == 16) 
                               {
                                 if (index == 15) index = 0 ;
                                 else index++;
                               }             
                            else index = but_stat;

                            // Clear interrupt number zone
                            copyXPMArea(4, 84, 13, 10, 46, 21);

                            // Display interrupt number
                            BlitNum (index, 52, 21);

	                  }
	                but_stat = -1;
	                break;
	        }
        }
	usleep(50000L);
      }

}

void GetRelease(LINUXVER *lver)
{
       struct utsname unameinfo;
       
       if ( uname(&unameinfo) == -1 ) 
                       perror("buffer wrong?");
               
       lver->major = atoi(strtok(unameinfo.release," ."));
       lver->minor = atoi(strtok(NULL," ."));
       lver->rev   = atoi(strtok(NULL, " ."));

}

void GetDatas20( void )
{
int i = 0, last;
FILE *fp;
char tokens[]={" :+"};
char buf[128];

fp = fopen("/proc/interrupts","r");
if (fp)
  {
   while ( (fgets(buf, 128, fp)) )
    {
      last = i;
      i = atoi ( strtok(buf, tokens) );

      // Update datas of missing interrupts 
      while ( ++last < i )
        {
          interrupts[last].bit = 0;
          interrupts[last].irqs = 0;
          free (interrupts[last].label); 
          interrupts[last].label = NULL;
        }
      interrupts[i].lastirqs = interrupts[i].irqs;
      interrupts[i].irqs = atoi ( strtok(NULL, tokens) );

      free (interrupts[i].label);
      interrupts[i].label = strdup (strtok(NULL, "")+2);

      interrupts[i].bit = ((interrupts[i].irqs - interrupts[i].lastirqs) != 0);
    }

   fclose (fp);
  }
else printf("Can't open /proc/interrupts.\n");
}

void GetDatas22( void )
{
int i=0, last;
FILE *fp;
char tokens[]={" :"};
char buf[128];

fp = fopen("/proc/interrupts","r");
if (fp)
  {
   fgets(buf, 128, fp);

   while ( (fgets(buf, 128, fp)) )
    {
      if ( *buf == 'N' ) break;

      last = i;
      i = atoi ( strtok(buf, tokens) );

      // Update datas of missing interrupts
      while ( ++last < i )
        {
          interrupts[last].bit = 0;
          interrupts[last].irqs = 0;
          free (interrupts[last].label);
          interrupts[last].label = NULL;
        }
      interrupts[i].lastirqs = interrupts[i].irqs;
      interrupts[i].irqs = atoi ( strtok(NULL, tokens) );

      strtok(NULL, tokens);
      free (interrupts[i].label);
      interrupts[i].label = strdup (strtok(NULL, "")+1);

      interrupts[i].bit = ((interrupts[i].irqs - interrupts[i].lastirqs) != 0);
    }

   fclose (fp);
  }
else printf("Can't open /proc/interrupts.\n");
}

// Blits a string at given co-ordinates
void BlitString(char *name, int x, int y)
{
    int		i;
    int		c;
    int		k;

    k = x;
    for (i=0; name[i]; i++)
    {

        c = toupper(name[i]); 
        if (c >= 'A' && c <= 'Z')
        {   // its a letter
			c -= 'A';
			copyXPMArea(c * 6, 74, 6, 8, k, y);
			k += 6;
        }
        else
        {   // its a number or symbol
			c -= '0';
			copyXPMArea(c * 6, 64, 6, 8, k, y);
			k += 6;
		}
	}

}


// Blits number to give coordinates.. nine 0's, right justified

void BlitNum(int num, int x, int y)
{
    char buf[1024];
    int newx=x;

    if ( num > 9 ) newx -= 6;
    if ( num > 99 ) newx -= 6;
    if ( num > 999 ) newx -= 6;
    if ( num > 9999 ) newx -= 6; 
    if ( num > 99999 ) newx -= 6;
    if ( num > 999999 ) newx -= 6;
    if ( num > 9999999 ) newx -= 6; 
    if ( num > 99999999 ) newx -= 6;
 
    sprintf(buf, "%i", num);

    BlitString(buf, newx, y);
}
    
// Split a label and display it

void DisplayLabel (char *label)
  {

    char *label1;
    char *label2;

    // Clear display zones
    copyXPMArea(4, 84, 39, 10, 4, 21);
    copyXPMArea(4, 84, 55, 10, 4, 35);

    if (label != NULL)
      {
        label1=strtok(label," ");
        label2=strtok(NULL," ");

        if (label2 != NULL) 
          {
            if (strlen(label1) <= 6)
              {
                BlitString (label1, 4, 21);
                BlitString (label2, 4, 35);
              }
            else
              {
                BlitString (label2, 4, 21);
                BlitString (label1, 4, 35);
              }
          } 
        else BlitString (label1, 4, 35);
      }
 }


/*******************************************************************************\
|* usage																	   *|
\*******************************************************************************/

void usage(void)
{
    fprintf(stderr, "\nwmint - S�bastien Li�nard <%s>, GCU (%s)\n\n",MY_EMAIL,GCU_HOME);
	fprintf(stderr, "usage:\n");
	fprintf(stderr, "\t-d <display name>\n");
        fprintf(stderr, "\t-i <interrupt number>\n");
	fprintf(stderr, "\t-h\tthis help screen\n");
	fprintf(stderr, "\t-v\tprint the version number\n");
	fprintf(stderr, "\n");
}

/*******************************************************************************\
|* printVersion																   *|
\*******************************************************************************/

void printVersion(void)
{
	fprintf(stderr, "wmint v%s\n", WMINT_VERSION);
}
/*****************************************************************************

   printLicence

******************************************************************************/

void printLicence(void)
{

printf(" wmint version %s -- Dockable interrupts monitor for Window Maker\n", WMINT_VERSION );
printf (" Copyright (C) 1999 S�bastien Li�nard <%s>\n", MY_EMAIL);
printf (" from GCU (%s)\n",GCU_HOME);
printf (" This software comes with NO WARRANTY: see the file COPYING for details.\n");
}
